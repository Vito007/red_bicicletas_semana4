var  mongoose = require("mongoose");
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;


var bicicletaSchema = new Schema({
   code: {
      type: Number,
      required: [true, 'El código es obligatorio'],
      unique: true
   },
   color: {
      type: String,
      trim: true,
      required: [true, 'El color es obligatorio']
   },
   modelo: {
      type: String,
      trim: true,
      required: [true, 'El modelo es obligatorio'] 
   },
   ubicacion: {
      type: [Number], 
      index: { type: '2dsphere', sparse: true }
   }
});

//Plugin para poder validar valores unicos
bicicletaSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe para otra bicicleta' });


bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
   return new this({
      code,
      color,
      modelo,
      ubicacion
   });
};



bicicletaSchema.methods.toString = function(){
   return `code: ${this.code} | color: ${this.color}`;
};

bicicletaSchema.statics.allBicis = function(cb){
   return this.find({}, cb);
}

bicicletaSchema.statics.add = function(aBici, cb){
   this.create(aBici, cb);
}

bicicletaSchema.statics.findByCode = function(aCode, cb){
   return this.findOne({code: aCode}, cb);
}

bicicletaSchema.statics.removeByCode = function(aCode, cb){
   return this.deleteOne({code: aCode}, cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);