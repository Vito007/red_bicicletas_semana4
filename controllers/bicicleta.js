var Bicicleta = require('../models/bicicleta');

module.exports = {
   list: function(req, res, next){
      Bicicleta.find({}, (err, bicicletas) => {
         res.render('bicicletas/index', { bicis: bicicletas, title: 'Bicicletas - Red Bicicletas' })
      });
   },
   create_get: function(req, res, next){
      res.render('bicicletas/create', { errors: {}, title: 'Crear Bicicleta - Red Bicicletas', bicicleta: new Bicicleta()});
   },
   create: function(req, res, next){
      Bicicleta.create({code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [ req.body.lat, req.body.lng ]}, function(err, nuevaBicicleta){
         if (err) {
            res.render('bicicletas/create', {errors: err.errors, title: 'Crear Bicicleta - Red Bicicletas', bicicleta: new Bicicleta({ code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [ req.body.lat, req.body.lng ] })});
         } else {
            res.redirect('/bicicletas');
         }
      });
   },
   update_get: function(req, res, next){
      Bicicleta.findById(req.params.id, function(err, bicicleta){
         res.render('bicicletas/update', { errors: {}, title: 'Actualizar Bicicleta - Red Bicicletas', bicicleta: bicicleta, title: 'Bicicletas - Red Bicicletas' })
      });
   },
   update: function(req, res, next){
      var update_values = {code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [ req.body.lat, req.body.lng ]};
      Bicicleta.findByIdAndUpdate(req.params.id, update_values, function(err, bicicleta){
         console.log(err);
         if(err) {
            // console.log(err);
            res.render('bicicletas/update', {errors: err.errors, title: 'Actualizar Bicicleta - Red Bicicletas', bicicleta: new Bicicleta({ code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [ req.body.lat, req.body.lng ] })});
         } else {
            res.redirect('/bicicletas');
            return;
         }
      });
   },
   delete: function(req, res, next){
      Bicicleta.findByIdAndDelete(req.body.id, function(err){
         if (err) 
            next(err);
         else
            res.redirect('/bicicletas');
      });
   }, 
   show: function(req, res, next) {
      Bicicleta.findById(req.params.id, function(err, bicicleta){
         res.render('bicicletas/show', { bicicleta: bicicleta, title: 'Mostrar Bicicleta - Red Bicicletas' });
      });
   }
}
// exports.bicicleta_list = function(req, res){
//    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
// }

// exports.bicicleta_create_get = function(req, res){
//    res.render('bicicletas/create');
// }

// exports.bicicleta_create_post = function(req, res){

//    var bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo});
//    bici.ubicacion = [req.body.lat, req.body.lng];
//    Bicicleta.add(bici);

//    res.redirect('/bicicletas');
// }

// exports.bicicleta_delete_post = function(req, res){
//    Bicicleta.removeById(req.body.id);

//    res.redirect('/bicicletas');
// }

// exports.bicicleta_update_get = function(req, res){
//    var bici = Bicicleta.findById(req.params.id);
//    res.render('bicicletas/update', {bici});
// }

// exports.bicicleta_update_post = function(req, res){
//    var bici = new Bicicleta.findById(req.params.id);
//    bici.id = req.body.id;
//    bici.color = req.body.color;
//    bici.modelo = req.body.modelo;
//    bici.ubicacion = [req.body.lat, req.body.lng];

//    res.redirect('/bicicletas');
// }


// exports.bicicleta_show = function(req, res){
//    var bici = Bicicleta.findById(req.params.id);
//    res.render('bicicletas/show', {bici});
// }
