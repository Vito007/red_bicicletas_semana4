var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {

   beforeEach(function(done){
      var mongoDB = 'mongodb://localhost:27017/testdb';
      mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

      const db = mongoose.connection;
      db.on('error', console.error.bind(console, 'MongoDB connection error: '));
      db.once('open', function(){
         console.log('We are connected to test database!');
         done();
      });
   });

   afterEach(function(done){
      Bicicleta.deleteMany({}, function(err, success){
         if (err) console.log(err);
         done();
      });
   });

   describe('GET BICICLETAS /', () => {
      it('Status 200', (done) => {
         request.get(base_url, function(error, response, body) {
            var result = JSON.parse(body);
            expect(response.statusCode).toBe(200);
            expect(result.bicicletas.lenght).toBe(0);
            done();
         });
      });
   });

   describe('POST BICICLETAS /create', () => {
      it('STATUS 200', (done) => {
         var headers = {'Content-Type:': 'application/json'};
         var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -25.292559, "lng": -57.621465}';
         request.post({
            headers: headers,
            url: base_url + '/create',
            body: aBici
         }, function(error, response, body){
            expect(response.statusCode).toBe(200);
            var bici = JSON.parse(body).bicicleta;
            console.log(bici);
            expect(bici.color).toBe("rojo");
            expect(bici.ubicacion[0]).toBe(-25.292559);
            expect(bici.ubicacion[1]).toBe(-57.621465);
            done();
         });
      });
   });
});
